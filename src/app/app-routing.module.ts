import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LecomponentComponent } from './lecomponent/lecomponent.component';
import { SignlistComponent } from './signlist/signlist.component';

const routes: Routes = [
	{ path : 'Signs', component : SignlistComponent},
	{ path : 'Reading', component : LecomponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ LecomponentComponent, SignlistComponent ];