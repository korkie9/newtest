import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignlistService {

  constructor() { }

  getList(){
  	return  [{"name" : "Aquarius January 20 - February 18"}, 
			{"name" : "Pisces February 19 - March 20"},
			{"name" : "Aries March 21 - April 19"},
			{"name" : "Taurus April 20 - May 20"},
			{"name" : "Gemini May 21 - June 20"},
			{"name" : "Cancer June 21 - July 22"},
			{"name" : "leo July 23 - August 22"},
			{"name" : "Virgo August 23 - September 22"},
			{"name" : "Libra September 23 - October 22"},
			{"name" : "Scorpio October 23 - November 21"},
			{"name": "Sagittarius November 22 - December 21"},
			{"name" : "Capricorn December 22 - January 19"}];
  }
}
