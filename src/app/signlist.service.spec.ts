import { TestBed } from '@angular/core/testing';

import { SignlistService } from './signlist.service';

describe('SignlistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignlistService = TestBed.get(SignlistService);
    expect(service).toBeTruthy();
  });
});
