import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LecomponentComponent } from './lecomponent/lecomponent.component';
import { LefindsignserviceService } from './lefindsignservice.service';
import { LefindfutureService } from './lefindfuture.service';
import { HttpClientModule } from '@angular/common/http';
import { routingComponents } from './app-routing.module';
import { SignlistComponent } from './signlist/signlist.component';
import { SignlistService } from './signlist.service';

@NgModule({
  declarations: [
    AppComponent,
    LecomponentComponent,
    SignlistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [LefindsignserviceService, LefindfutureService, SignlistService],
  bootstrap: [AppComponent]
})
export class AppModule { }
