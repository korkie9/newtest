import { TestBed } from '@angular/core/testing';

import { LefindsignserviceService } from './lefindsignservice.service';

describe('LefindsignserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LefindsignserviceService = TestBed.get(LefindsignserviceService);
    expect(service).toBeTruthy();
  });
});
