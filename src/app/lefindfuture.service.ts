import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { leinterface } from './leinterface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LefindfutureService {

  constructor(private http : HttpClient) { }

  private _url : string = "../assets/data/predictions.json";

  getPredictions() :  Observable<leinterface[]>{
  	return this.http.get<leinterface[]>(this._url);
  }
}
