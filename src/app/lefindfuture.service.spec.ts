import { TestBed } from '@angular/core/testing';

import { LefindfutureService } from './lefindfuture.service';

describe('LefindfutureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LefindfutureService = TestBed.get(LefindfutureService);
    expect(service).toBeTruthy();
  });
});
