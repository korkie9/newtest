export class lefindsign {

	constructor(){}

	public sign : string;

	public findsign(day : number, month : string){

		switch(month){
  		
  		case "January": {
  			if(day >= 1 || day <=19){
  				return "Capricorn";
  			} else if(day >= 20 || day <= 31){
  				return "Acquarius";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "February": {
  			if(day >= 1 || day <=18){
  				return "Aquarius";
  			} else if(day >= 19 || day <= 30){
  				return "Pisces";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "March": {
  			if(day >= 1 || day <=20){
  				return "Pisces";
  			} else if(day >= 21 || day <= 31){
  				return "Aires";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "April": {
  			if(day >= 1 || day <= 19){
  				return "Aires";
  			} else if(day >= 20 || day <= 30){
  				return "Tourus";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "May": {
  			if(day >= 1 || day <= 20){
  				return "Tourus";
  			} else if(day >= 21 || day <= 31){
  				return "Gemini";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "June": {
  			if(day >= 1 || day <= 20){
  				return "Gemini";
  			} else if(day >= 21 || day <= 30){
  				return "Cancer";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "July": {
  			if(day >= 1 || day <= 22){
  				return "Cancer";
  			} else if(day >= 23 || day <= 31){
  				return "Leo";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "August": {
  			if(day >= 1 || day <= 22){
  				return "Leo";
  			} else if(day >= 23 || day <= 31){
  				return "Virgo";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "September": {
  			if(day >= 1 || day <= 22){
  				return "Virgo";
  			} else if(day >= 23 || day <= 30){
  				return "Libra";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "October": {
  			if(day >= 1 || day <= 22){
  				return "Libra";
  			} else if(day >= 23 || day <= 31){
  				return "Scorpio";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "November": {
  			if(day >= 1 || day <= 21){
  				return "Scorpio";
  			} else if(day >= 22 || day <= 30){
  				return "Sagitarius";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "December": {
  			if(day >= 1 || day <= 21){
  				return "Sagitarius";
  			} else if(day >= 22 || day <= 31){
  				return "Capricorn";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		default : {
  			console.log("Not a thing");
  			alert("Please enter a correct month. I don't know what you entered but it definitely isn't a month. And don't forget your capital letters. Spelling is important.");
  			break;
  		}
  	}
	}
}