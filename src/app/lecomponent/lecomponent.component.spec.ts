import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LecomponentComponent } from './lecomponent.component';

describe('LecomponentComponent', () => {
  let component: LecomponentComponent;
  let fixture: ComponentFixture<LecomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LecomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
