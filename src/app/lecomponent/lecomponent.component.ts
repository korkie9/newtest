import { Component, OnInit } from '@angular/core';
import { LefindsignserviceService } from '../lefindsignservice.service';
import { LefindfutureService } from '../lefindfuture.service';

@Component({
  selector: 'app-lecomponent',
  templateUrl: './lecomponent.component.html',
  styleUrls: ['./lecomponent.component.css']
})
export class LecomponentComponent implements OnInit {

  	public myFuture : string;
  	public sign  : string = "";
  	public fortune = [];

  constructor(public signer : LefindsignserviceService, private predictionService : LefindfutureService){}
   

  getFuture(day : number, month : string){
  	this.sign = this.signer.findsign(day, month);
  	console.log(this.sign);
  	this.myFuture = this.fortune[Math.floor(Math.random() * 37)];
  }

  ngOnInit() {
  	this.predictionService.getPredictions()
  	.subscribe(data => this.fortune = data);

  }

}
