import { TestBed } from '@angular/core/testing';

import { LepredictionsService } from './lepredictions.service';

describe('LepredictionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LepredictionsService = TestBed.get(LepredictionsService);
    expect(service).toBeTruthy();
  });
});
