import { Component, OnInit } from '@angular/core';
import { SignlistService } from '../signlist.service';

@Component({
  selector: 'app-signlist',
  templateUrl: './signlist.component.html',
  styleUrls: ['./signlist.component.css']
})
export class SignlistComponent implements OnInit {

	public zodiacs = [];

  constructor(public zodiacSigns : SignlistService) {}

  ngOnInit() {
  	this.zodiacs = this.zodiacSigns.getList();
  }



}
