import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LefindsignserviceService {

	constructor(){}

	public findsign(day : number, month : string){

		switch(month.toLowerCase()){
  		
  		case "january": {
  			if(day >= 1 && day <=19){
  				return "Capricorn";
  			} else if(day >= 20 && day <= 31){
  				return "Acquarius";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "february": {
  			if(day >= 1 && day <=18){
  				return "Aquarius";
  			} else if(day >= 19 && day <= 30){
  				return "Pisces";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "march": {
  			if(day >= 1 && day <=20){
  				return "Pisces";
  			} else if(day >= 21 && day <= 31){
  				return "Aires";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "april": {
  			if(day >= 1 && day <= 19){
  				return "Aires";
  			} else if(day >= 20 && day <= 30){
  				return "Tourus";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "may": {
  			if(day >= 1 && day <= 20){
  				return "Tourus";
  			} else if(day >= 21 && day <= 31){
  				return "Gemini";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "june": {
  			if(day >= 1 && day <= 20){
  				return "Gemini";
  			} else if(day >= 21 && day <= 30){
  				return "Cancer";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "july": {
  			if(day >= 1 && day <= 22){
  				return "Cancer";
  			} else if(day >= 23 && day <= 31){
  				return "Leo";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "august": {
  			if(day >= 1 && day <= 22){
  				return "Leo";
  			} else if(day >= 23 && day <= 31){
  				return "Virgo";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "september": {
  			if(day >= 1 && day <= 22){
  				return "Virgo";
  			} else if(day >= 23 && day <= 30){
  				return "Libra";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "october": {
  			if(day >= 1 && day <= 22){
  				return "Libra";
  			} else if(day >= 23 && day <= 31){
  				return "Scorpio";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "november": {
  			if(day >= 1 && day <= 21){
  				return "Scorpio";
  			} else if(day >= 22 && day <= 30){
  				return "Sagittarius";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		case "december": {
  			if(day >= 1 && day <= 21){
  				return "Sagittarius";
  			} else if(day >= 22 && day <= 31){
  				return "Capricorn";
  			} else {
  				alert("The day you entered is either too large or too small. Do something about that.");
  			}
  			break;
  		}

  		default : {
  			console.log("Not a thing");
  			alert("Please enter a correct month. I don't know what you entered but it definitely isn't a month. And don't forget, spelling is important.");
  			break;
  		}
  	}
	}
}
